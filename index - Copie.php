<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;
use crazy_cd\vue\Vue;
use crazy_cd\bd\Eloquent;

$db=Eloquent::load();

$app = new \Slim\Slim();

	//Par défault
	$app->get('/', function() {
		(new Vue())->render(Vue::PageAcceuil);
	})->name('route_defaut');
	//Direction page de connexion
	$app->get('/pageConnexion', function() {
		(new Vue())->render(Vue::PageConnexion);
	})->name('route_page_connexion');
	//Direction page d'inscription
	$app->get('/pageInscription', function() {
		(new Vue())->render(Vue::PageInscription);
	})->name('route_page_inscription');

$app->run();
