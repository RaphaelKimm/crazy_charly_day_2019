<?php

namespace crazy_cd\vue;

use crazy_cd\models\Offre;
use crazy_cd\models\Categorie;

	class OffreView extends View{
		public function render($selecteur){
			//switch sur le selecteur
			switch($selecteur){
				case 'index':
					$content = $this->index();
				break;
				case 'creationOffre':
					$content = $this->contenuCreationOffre();
				break;
				case 'offre':
					$content = $this->contenuOffre();
				break;
				default :
					$content ="";
				break;
			}
			//retour
			echo $this->html($content);
		}

		public function index(){
			$content2="<div class=\"offre\">";

			foreach($this->var as $offre){
				$categs=$offre->categories()->first();
				$employeur=$offre->employeur()->first();
				$url=\Slim\Slim::getInstance()->urlFor('afficher_offre', ["id"=>$offre->id]);
				$content2.= <<<END
				<div class="boiteOffre">
						<h3><a href="$url">$offre->titre</a></h3>

						<p>$offre->description</p>

						<p>à : $offre->lieu</p>

						<p>postée le : $offre->creation</p>

						<p>par : $employeur->nom</p>

						<p>catégories : $categs->nom</p>
				</div>
END;
			}

			return $content2."</div>";
		}
		
		public function contenuCreationOffre(){
		$app = \Slim\Slim::getInstance();
		$url = \Slim\Slim::getInstance()->urlFor('route_creation_offre');
			
		$listeCategories = "<select name=\"categorieOffre\">\n";
		$categories = Categorie::all();
		foreach ($categories as $categorie) {
			$listeCategories .= "\t<option value=".$categorie->id.">".$categorie->nom."</option>\n";
		}
		$listeCategories .= "</select>";
			
		$res = <<<END

	<div class="container">
		<div class="row centered">
			<div class="col">
				<form id="createoffre" method="post">
					<div class="form-group">
						<label>Titre</label>
						<input class="form-control" type="text" name="titreOffre">
					</div>
					<div class="form-group">
						<label>Catégorie</label>
						$listeCategories
					</div>
					<div class="form-group">
						<p>Description</p>
						<textarea class="form-control" rows=10 name="descriptionOffre"></textarea>
					</div>
					<div class="form-group">
						<label>Lieu de travail</label>
						<input class="form-control" type="text" name="lieuOffre">
					</div>
					<div class="form-group">
						<label>Tag 1</label>
						<input class="form-control" type="text" name="tag1Offre">
					</div>
					<div class="form-group">
						<label>Tag 2</label>
						<input class="form-control" type="text" name="tag2Offre">
					</div>
					<div class="form-group">
						<label>Tag 3</label>
						<input class="form-control" type="text" name="tag3Offre">
					</div>
					<button class="bouton" type="submit" formaction="$url"name="creer_liste" value="valid_f1">valider</button>
				</form>
			</div>
		</div>
	</div>
END;
		return $res;
		}
		
		

		public function contenuOffre(){
			$offre = $this->var;

			$categorie=$offre->categories()->first();
			$employeur=$offre->employeur()->first();
			$content2 = <<<END
				<div class="boiteOffre">
					<h3>$offre->titre</h3>

					<p>$offre->description</p>

					<p>à : $offre->lieu</p>

					<p>postée le : $offre->creation</p>

					<p>par : $employeur->nom</p>

					<p>catégories : $categorie->nom</p>
END;
			//tags
			$content2.="<p>tags :</p>\n<span>";
			foreach($offre->tags()->get() as $tag)
				$content2.="<p class=\"tag\">$tag->texte</p>";
				
			$content2.="</span>";

			return $content2."</div>";
			}
		}
?>