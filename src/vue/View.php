<?php

namespace crazy_cd\vue;

use wishlist\classes\Authentification as Auth;

abstract class View {

	/**
	 * La(Les) variable(s) de la page
	 * @var array
	 */
	protected $var;

	/**
	 * Contenu
	 * @var string
	 */
	protected $content;

	/**
	 * Slim
	 * @var \Slim\Slim
	 */
	protected $app;

	private $nav;

	public function __construct($var = null) {
		$this->var = $var;
		$this->content = "";
		$this->app = \Slim\Slim::getInstance();
	}

	public abstract function render($view);
	/**
	* Fonction qui renvoit un corps HTML remplit
	* Il prend en parametre un remplissage HTML
	*/
	public function html($content) {
		//Route pour aller chercher le css
		$rootCSS = \Slim\Slim::getInstance()->request()->getRootUri();
		$rootURL=$rootCSS;
		$rootCSS.= "/src/css/css.css";

		$rootJS=\Slim\Slim::getInstance()->request()->getRootUri();
		$rootJSVue=$rootJS."/node_modules/vue/dist/vue.min.js";
		$rootJS.="/front/js/main.js";

		//var_dump($rootCSS);
		//Le corps de base html
		$res = <<<END
		<!DOCTYPE html>
					<html>
						<head>
							<title>Handi Interim</title>
							<meta charset="UTF-8">
							<link rel="stylesheet" type="text/css" href=$rootCSS>
						</head>
						
						<body>
						<div id="app">
							
							<header>
								<nav id="nav">
									<ul v-if="seen" id="nav_list">
										<li>
											<a href=$rootURL/>Acceuil</a>
											<a href=$rootURL/offre/index>Offres d'emploi</a>
											<a href=$rootURL/offre/creation>créer une Offre d'emploi</a>
										</li>
									</ul>
								</nav>
								
							</header>	
							
							$content

							<footer>
								<h2>Credit</h2>
								<div>
									<div>
										Hodot Arthur - Risacher Jeremy - Battani Lilian - Raphael Kimm - Xiechen Zhang
									</div>
								</div>
							</footer>
							
							<script type=module src=$rootJS></script>
							<script src=$rootJSVue></script>
						</div>
						</body>
					</html>
END;
	return $res;
	}

}
