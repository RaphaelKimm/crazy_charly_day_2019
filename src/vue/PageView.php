<?php

namespace crazy_cd\vue;

class PageView extends View{

	const PageAcceuil = 0, Connexion = 1, Inscription = 2, Message=3;

	public function render($selecteur,$donnees=null){
		//switch sur le selecteur
		switch($selecteur){
			case self::PageAcceuil:
				$content = $this->ContenueAccueil();
				break;
			case self::Connexion:
				$content = $this->ContenuePageConnexion($donnees);
				break;
			case self::Inscription:
				$content = $this->ContenuePageInscription($donnees);
				break;
			case self::Message:
				$content= $this->afficherMessage($donnees);
				break;
			default :
				$content ="";
				break;
		}
		//retour
		echo $this->html($content);
	}
	/**
	* Fonction qui renvoit le contenue de l'accueil
	*/
	private function ContenueAccueil(){
		$app = \Slim\slim::getInstance();
		$connexion = $app->urlFor('route_page_connexion');

		$inscription = $app->urlFor('route_page_inscription');
		$offres = $app->urlFor('route_index_offres');

		$deconnexion=$app->urlFor('route_deconnexion');
		$sup="";
		if (isset($_SESSION["usernameConnexion"])) {
			$sup="<button class='bouton' formaction='$deconnexion' >Déconnexion</button>\n";
		}
		else {
			$sup="<button class='bouton' formaction='$connexion' >Connexion</button>\n";
		}
		$sup.="<button class='bouton' formaction='$inscription' >Inscription</button>\n";
		$res = <<<END
			<div>
					<h1 class="boiteTitre">Bienvenue sur Handi Interim</h1>
			</div>
			<form>
					$sup
			</form>
END;
		return $res;
	}


	/**
	*
	*/
	private function ContenuePageConnexion($donnees){
		$sup="";
		if (isset($donnees["erreur"])) {
			$sup=$donnees["erreur"];
		}
		else if (isset($_SESSION["username"])) {
			$sup=$_SESSION["username"];
		}
		$res = <<<END
			<div id='connexion'>
			<form method='GET'>\n
				<div id="connexion">\n
					<label>Entrez votre identifiant</label>\n
					<input class="form-control" type="text" name="username" required>\n
					<label>Entrez votre mot de passe</label>\n
					<input class="form-control" type="password" name="password" required>\n
					<button @click="visible()" type="submit">Valider</button>\n


					
				</div>\n


			</form>\n
			
			</div>

END;
		return $res;
	}

	/**
	*
	*/
	private function ContenuePageInscription($donnees){
		//TODO
		//rajouter des vrais champs !

		$slim=\Slim\Slim::getInstance();
		$erreur="";
		if (isset($donnees["erreur"]))
		{
			$erreur=$donnees["erreur"];
		}
		$res = <<<END

			<form id="inscription" method="POST" action=>\n
					<label>Choisissez votre identifiant</label><br/>\n
					<input class="form-control" type="text" name="username" required><br/>\n
					<label>Choisissez votre mot de passe</label><br/>\n
					<input class="form-control" type="password" name="password" required><br/>\n
					<label>Retaper votre mot de passe</label><br/>\n
					<input class="form-control" type="password" name="passwordVerify" required><br/>\n
					<button class="bouton" type="submit">Valider</button><br/>\n
					$erreur
			</form>\n
END;
		return $res;
	}

	/**
	 * Methode renvoyant un message spécifique
	 * @param $msg String, chaine (message)
	 */
	public function afficherMessage($donnees) {
			$sup="";
			$message="";

			if (isset($donnees["message"])) {
				$message=$donnees["message"];
			}
			else if (isset($_SESSION["usernameConnexion"])) {
				$message="Vous êtes connecté, ".$_SESSION["usernameConnexion"];
			}
			$app = \Slim\Slim::getInstance();
			$accueil = $app->urlFor("route_defaut");
			$res = <<<END
				<div id="message">\n
					<h2 class="boiteOffre">$message<br>Retournez à l'accueil<br></h2>
				</div>\n
END;
		return $res;
	}

}
