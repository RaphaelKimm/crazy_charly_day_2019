<?php 

namespace crazy_cd\models;

class Candidature extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'candidature';
	protected $primaryKey = 'id' ;
	public $timestamps = false ;
	
}
