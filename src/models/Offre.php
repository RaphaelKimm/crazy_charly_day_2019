<?php 

namespace crazy_cd\models;

use crazy_cd\models\Decrit;
use Illuminate\Database\Eloquent\Model;

class Offre extends Model {
	
	protected $table = 'offre';
	protected $primaryKey = 'id' ;
	public $timestamps = false ;
	
	public function categories(){
		return $this->belongsTo('crazy_cd\models\Categorie','categorie');
	}
	
	public function employeur(){
		return $this->belongsTo('crazy_cd\models\User','employeur');
	}
	
	public function tags(){
		return $this->belongsToMany('crazy_cd\models\Tag','decrit');
	}
}