<?php 

namespace crazy_cd\models;

class PropoTransport extends \Illuminate\Database\Eloquent\Model {
	
	protected $table = 'propotransport';
	protected $primaryKey = 'id' ;
	public $timestamps = false ;
	
}