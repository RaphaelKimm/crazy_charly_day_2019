<?php
namespace crazy_cd\controller;
/**
 * classe de base servant de modèle aux autres controllers
 */
abstract class Controller {

	protected $app;

	public function __construct() {
		$this->app = \Slim\Slim::getInstance();
	}
	/**
	 * not found 404
	 */
	public function notFound() {
		http_response_code(404);
		die;
	}

}
