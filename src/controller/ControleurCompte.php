<?php

namespace crazy_cd\controller;

use crazy_cd\models\User;

use crazy_cd\vue\PageView;

class ControleurCompte {

  public function register() {
    $slim=\Slim\Slim::getInstance();
      $username=filter_var($_POST['username'],FILTER_SANITIZE_STRING);
      $password=password_hash(filter_var($_POST['password'],FILTER_SANITIZE_STRING),PASSWORD_BCRYPT,["cost" => 4]);



      $exist=User::where('nom','=',$username)->first();

      $v = new PageView();

      if (!$exist && $_POST['password']===$_POST['passwordVerify']) {
        $url=$slim->urlFor('route_defaut');
        $user=new User();
        $user->nom=$username;
        $user->mdp=$password;
        $user->administrateur=0;
        $user->save();

        $donnees["type"]="Succès";
        $donnees["message"]="Votre inscription a bien été prise en compte";

        $v->render(PageView::Message,$donnees);
      }
      else if ($_POST['password']!==$_POST['passwordVerify']){

        $donnees["type"]="Echec";
        $donnees["erreur"]="Le mot de passe doit être identique";

        $v->render(PageView::Inscription,$donnees);
      }
      else {
        $donnees["type"]="Echec";
        $donnees["erreur"]="L'utilisateur $username existe déjà";
        $v->render(PageView::Inscription,$donnees);
      }

  }

  public function login() {
    $v = new PageView();
    if (isset($_GET["usernameConnexion"])) {
      $username=filter_var($_GET['usernameConnexion'],FILTER_SANITIZE_STRING);
      $password=filter_var($_GET['passwordConnexion'],FILTER_SANITIZE_STRING);
      $user=User::where("nom","=",$username)->first();
      if (isset($user)) {
        if (password_verify($password,$user->mdp))
        {
          $_SESSION["usernameConnexion"]=$username;
          $_SESSION["idConnexion"]=$user->id;
          $v->render(PageView::Message);
        }
        else {
          $donnees["erreur"]="Mot de passe incorrect";
          $v->render(PageView::Connexion,$donnees);
        }
      }
      else {
        $donnees["erreur"]="Utilisateur inexistant";
        $v->render(PageView::Connexion,$donnees);
      }
    }
    else {
      $v->render(PageView::Connexion);
    }
  }

  public function logout() {
    $v=new PageView();
    $username=$_SESSION["usernameConnexion"];
    unset($_SESSION["usernameConnexion"]);
    unset($_SESSION["idConnexion"]);
    $donnees["message"]="Vous avez été déconnecté, $username";
    $v->render(PageView::Message,$donnees);
  }
}
