<?php
	namespace crazy_cd\controller;

	use crazy_cd\vue\OffreView;
	use crazy_cd\models\Offre;
	use crazy_cd\models\Tag;
	use crazy_cd\models\Decrit;
	/**
	 * controller des offres d'emploi
	 */
	class OffreController extends Controller{
		/**
		 * affiche toutes les offres d'emploi
		 */
		public function index(){
			$offres=Offre::all();
			$offre_view=new OffreView($offres);
			$offre_view->render('index');
		}
	
		/**
		 * Methode pour afficher la page d'ajout d'une offre.
		 *
		 * @domain public
		 */
		public function afficherAjoutOffre(){
			$app = \Slim\Slim::getInstance();
			$offre_view=new OffreView(null);
			$offre_view->render('creationOffre');
		}
		
		/**
		 * Methode pour ajouter une offre.
		 *
		 * @domain public
		 */
		public function ajoutOffre(){
			if (isset($_SESSION["idConnexion"])) {
				$app = \Slim\Slim::getInstance();
				$offre = new Offre();
				$offre->titre = $app->request()->post('titreOffre');
				$offre->description = $app->request()->post('descriptionOffre');
				$offre->lieu = $app->request()->post('lieuOffre');
				$offre->creation = date('Y-m-d');
				$offre->categorie = $app->request()->post('categorieOffre');
				$offre->employeur = $_SESSION["idConnexion"]; //todo avec les sessions
				$offre->save();
				
				//tags
				for ($i=1; $i<=3; $i++) {
					$tag = $app->request()->post('tag'.$i.'Offre');
					if ($tag!='') { //un tag a été écrit
						$tagId = -1;
						if (Tag::where('texte','like',$tag)->count()==0) { //le tag n'existe pas
							$tagItem = new Tag();
							$tagItem->texte = $tag;
							$tagItem->save();
							$tagId = $tagItem->id;
						}
						else { //le tag existe
							$tagId = Tag::select('id')->where('texte','like',$tag)->first()->id;
						}
						$decrit = new Decrit();
						$decrit->offre_id = $offre->id;
						$decrit->tag_id = $tagId;
						$decrit->save();
					}
				}
				
				//renvoit à l'offre
				$url = \Slim\Slim::getInstance()->urlFor('afficher_offre', ['id' => $offre->id]);
				header("Location: ".$url);
				exit();
			}
			else {
				$url = \Slim\Slim::getInstance()->urlFor('route_defaut');
				header("Location: ".$url);
				exit();
			}
		}
		
		/**
		 * Methode pour afficher une offre
		 */
		public function afficherOffre($id){
			$app = \Slim\Slim::getInstance();
			$offre_view=new OffreView(Offre::find($id));
			$offre_view->render('offre');
		}
	}
?>