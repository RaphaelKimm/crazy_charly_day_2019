-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mer 06 Février 2019 à 15:38
-- Version du serveur :  5.7.25-0ubuntu0.18.04.2
-- Version de PHP :  7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ccd2019`
--

-- --------------------------------------------------------

--
-- Réinitialisation des tables (en cas de changement)
--

DROP TABLE IF EXISTS `propotransport`;
DROP TABLE IF EXISTS `candidature`;
DROP TABLE IF EXISTS `decrit`;
DROP TABLE IF EXISTS `tag`;
DROP TABLE IF EXISTS `offre`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `categorie`;

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`nom`) VALUES
('Administratif : assistant comptable'),
('Administratif : comptable'),
('Administratif : secrétaire'),
('Administratif : standardiste'),
('Administratif : comptable'),
('Administratif : secrétaire'),
('Administratif : standardiste'),
('Bâtiment/Travaux Publics : conducteur d\'engin'),
('Bâtiment/Travaux Publics : manœuvre'),
('Bâtiment/Travaux Publics : maçon'),
('Bâtiment/Travaux Publics : électricien'),
('Commerce et vente : assistant commercial'),
('Commerce et vente : commercial'),
('Commerce et vente : manager'),
('Commerce et vente : vendeur polyvalent'),
('Logistique : magasinier'),
('Logistique : préparateur de commandes'),
('Restauration et hôtellerie : aide cuisinier'),
('Restauration et hôtellerie : cuisinier'),
('Restauration et hôtellerie : employé polyvalent'),
('Restauration et hôtellerie : serveur'),
('Transport : cariste'),
('Transport : chauffeur de bus'),
('Transport : conducteur poids lourd'),
('Transport : livreur');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) CHARACTER SET utf8 NOT NULL,
  `mdp` varchar(256) NOT NULL,
  `administrateur` boolean DEFAULT FALSE,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`nom`, `mdp`) VALUES
('Cassandre','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Achille','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Calypso','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Bacchus','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Diane','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Clark','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Helene','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Jason','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Bruce','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Pénélope','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Ariane','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO'),
('Lois','$2y$12$ZQp61TjRJR273VaQKAM5ee/tRZMG0Xb.GVcdw2nhAIR4vNN/IH3IO');

-- --------------------------------------------------------

--
-- Structure de la table `offre`
--

CREATE TABLE `offre` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`employeur` int(11) NOT NULL,
	`categorie` int(11) NOT NULL,
	`titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`description` text COLLATE utf8_unicode_ci,
	`lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`creation` date NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`employeur`) REFERENCES user(`id`),
	FOREIGN KEY (`categorie`) REFERENCES categorie(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `offre`
--

INSERT INTO `offre` (`employeur`, `categorie`,`titre`,`description`,`lieu`,`creation`) VALUES
(1, 19, 'Cuisinier pour l\'hôtel 2 étoiles UNNAMED', 'Recherche d\'un cuisinier pour le restaurant de l\'hôtel UNNAMED de NOWHERE.\nExpérience exigée :\n\tFormation de cuisinier depuis 5 ans minimum.\n\nUn test sera passé au candidat dans nos cuisines pour vérifier ses compétences.', '2 rue NOWHERE', '2019-01-17'),
(3, 6, 'Secrétaire (M/F) pour aceuil à une station balnéaire', 'Nous sommes à la recherche d\'un/une secretaire pour gérer l\'accueil des touristes à notre station balnéaire.', '10 rue NOWHERE', '2019-01-16');

-- --------------------------------------------------------

--
-- Structure de la table `tag` (représentation des tags (ou caractéristiques) des offres d'emplois)
--

CREATE TABLE `tag` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`texte` varchar(30) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tag`
--

INSERT INTO `tag` (`texte`) VALUES
('HÔTEL 2 ÉTOILES'),
('CUISINER'),
('STATION BALNÉAIRE'),
('ACCUEIL'),
('CONTACT AVEC CLIENTS');

-- --------------------------------------------------------

--
-- Structure de la table `decrit` (association d'un tag à une offre)
--

CREATE TABLE `decrit` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`tag_id` int(11) NOT NULL,
	`offre_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`offre_id`) REFERENCES offre(`id`),
	FOREIGN KEY (`tag_id`) REFERENCES tag(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `decrit`
--

INSERT INTO `decrit` (`tag_id`, `offre_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 2),
(5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `candidature`
--

CREATE TABLE `candidature` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`offre` int(11) NOT NULL,
	`candidat` int(11) NOT NULL,
	`depart` varchar(255) NOT NULL,
	`creation` date NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`candidat`) REFERENCES user(`id`),
	FOREIGN KEY (`offre`) REFERENCES offre(`id`)
	
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `candidature`
--

INSERT INTO `candidature` (`offre`, `candidat`, `depart`, `creation`) VALUES
(2, 8, '10 bis rue NOWHERE', '2019-02-01');

-- --------------------------------------------------------

--
-- Structure de la table `propotransport`
--

CREATE TABLE `propotransport` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`candidature` int(11) NOT NULL,
	`conducteur` int(11) NOT NULL,
  PRIMARY KEY (`id`),
	FOREIGN KEY (`candidature`) REFERENCES candidature(`id`),
	FOREIGN KEY (`conducteur`) REFERENCES user(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `propotransport`
--

-- --------------------------------------------------------

--
-- Index pour les tables exportées
--
/*
--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `offre`
--
ALTER TABLE `offre`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tag`
--
ALTER TABLE `tag`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `decrit`
--
ALTER TABLE `decrit`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `candidature`
--
ALTER TABLE `candidature`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `propotransport`
--
ALTER TABLE `propotransport`
  ADD PRIMARY KEY (`id`);
*/
-- --------------------------------------------------------
/*
--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `offre`
--
ALTER TABLE `offre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `tag`
--
ALTER TABLE `tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `tag`
--
ALTER TABLE `decrit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `candidature`
--
ALTER TABLE `candidature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `propotransport`
--
ALTER TABLE `propotransport`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
  */
-- --------------------------------------------------------
/*
--
-- clefs etrangeres pour les tables exportées
--

--
-- clefs etrangeres pour la table `offre`
--
ALTER TABLE `offre`
	ADD FOREIGN KEY (`employeur`) REFERENCES user(`id`);
ALTER TABLE `offre`
	ADD FOREIGN KEY (`categorie`) REFERENCES categorie(`id`);
	
--
-- clefs etrangeres pour la table `decrit`
--
ALTER TABLE `decrit`
	ADD FOREIGN KEY (`offre_id`) REFERENCES offre(`id`);
ALTER TABLE `decrit`
	ADD FOREIGN KEY (`tag_id`) REFERENCES tag(`id`);
	
--
-- clefs etrangeres pour la table `candidature`
--
ALTER TABLE `candidature`
	ADD FOREIGN KEY (`candidat`) REFERENCES candidat(`id`);
ALTER TABLE `candidature`
	ADD FOREIGN KEY (`offre`) REFERENCES offre(`id`);
	
--
-- clefs etrangeres pour la table `propotransport`
--
ALTER TABLE `propotransport`
	ADD FOREIGN KEY (`candidature`) REFERENCES candidature(`id`);
ALTER TABLE `propotransport`
	ADD FOREIGN KEY (`conducteur`) REFERENCES user(`id`);
  */
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
