<?php

namespace crazy_cd\bd;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Cette classe permet d'initialiser l'ORM Eloquent du framework Laravel.
 * Elle permet par consequent d'initialiser la connexion a la base de donnees MySQL en respectant le principe MVC.
 */
class Eloquent {

  public static $db;

  public static function load()
  {
    self::$db = new DB();
    self::$db->addConnection(parse_ini_file("src/conf/conf.ini"));
    self::$db->setAsGlobal();
    self::$db->bootEloquent();
  }
}
