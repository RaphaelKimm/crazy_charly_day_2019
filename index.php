<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

use crazy_cd\bd\Eloquent;
use crazy_cd\vue\PageView;
use crazy_cd\controller\OffreController;
use crazy_cd\controller\ControleurCompte;

$db=Eloquent::load();


$app = new \Slim\Slim();

session_start();

	//Par défault
	$app->get('/', function() {
		(new PageView())->render(PageView::PageAcceuil);
	})->name('route_defaut');
	//Direction page de connexion
	$app->get('/pageConnexion', function() {
		$c = new ControleurCompte();
		$c->login();
	})->name('route_page_connexion');
	//Direction page d'inscription
	$app->get('/pageInscription', function() {
		(new PageView())->render(PageView::Inscription);
	})->name('route_page_inscription');
	$app->post('/pageInscription', function() {
		$c=new ControleurCompte();
		$c->register();
	});

	//Direction page d'offre d'emploi
	$app->get('/offre/index', function() {
		(new OffreController())->index();
	})->name('route_index_offres');
	//Direction page d'une offre d'emploi
	$app->get('/offre/consulter/:id', function($id) {
		(new OffreController())->afficherOffre($id);
	})->name('afficher_offre');
	//Direction création d'offre d'emploi
	$app->get('/offre/creation', function() {
		(new OffreController())->afficherAjoutOffre();
	})->name('route_creation_offre');
	$app->post('/offre/creation', function() {
		(new OffreController())->ajoutOffre();
	});

	// Direction deconnexion
	$app->get('/deconnexion', function() {
		$c= new ControleurCompte();
		$c->logout();
	})->name('route_deconnexion');

$app->run();
