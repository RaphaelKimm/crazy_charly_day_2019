/**
 * constructeur pour la barre de navigation
 * utilise la vue associé pour les manipulations
 */
let seen=false;
let v_view;

 function NavBar(view){
 	v_view=view;	
 }
 /**
  * rend la nav bar visible
  */
  NavBar.prototype.toggleVisible=function(){
  		seen=true;
  		v_view.seen=seen;
  }
  /**
   * cache la nav bar visible
   */
   NavBar.prototype.hide=function(){
   		seen=false;
  		v_view.seen=seen;
   }
   /**
    * export la NavBar
    */
    export NavBar;
