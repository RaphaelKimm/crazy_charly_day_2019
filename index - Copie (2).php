<?php

require_once 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

use crazy_cd\bd\Eloquent;
use crazy_cd\vue\PageView;
use crazy_cd\controller\OffreController;

$db=Eloquent::load();

$app = new \Slim\Slim();

	//Par défault
	$app->get('/', function() {
		(new PageView())->render(PageView::PageAcceuil);
	})->name('route_defaut');
	//Direction page de connexion
	$app->get('/pageConnexion', function() {
		(new PageView())->render(PageView::PageConnexion);
	})->name('route_page_connexion');
	//Direction page d'inscription
	$app->get('/pageInscription', function() {
		(new PageView())->render(PageView::PageInscription);
	})->name('route_page_inscription');
	//Direction page d'offre d'emploi
	$app->get('/offre/index', function() {
		(new OffreController())->index();
	})->name('route_index_offres');
	//Message spécifique
	$app->post("/inscription", function() {
		$c = new ControleurCompte();
		$c->register();
	});

$app->run();
